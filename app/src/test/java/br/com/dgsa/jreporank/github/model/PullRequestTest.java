package br.com.dgsa.jreporank.github.model;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("unused")
@RunWith(JUnit4.class)
public class PullRequestTest {

    @SuppressWarnings("TooBroadScope")
    @Test
    public void mustBeDeserializedCorrectly() {
        String json = "{title:\"PR Title\", created_at:\"2016-05-30T11:20:36Z\", " +
                "body:\"the body may be too long\", html_url:\"google.com.br\", " +
                "user:{login:\"auth\", avatar_url:\"auth.url.com.br\"}}";
        String expectedTitle = "PR Title";
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT-0"));
        calendar.set(2016, 4, 30, 11, 20, 36);
        calendar.set(Calendar.MILLISECOND, 0);
        Date expectedDate = calendar.getTime();
        String expectedBody = "the body may be too long";
        String expectedAuthName = "auth";
        String expectedAuthAvatarUrl = "auth.url.com.br";
        String expectedUrl = "google.com.br";

        PullRequest pullRequest = new Gson().fromJson(json, PullRequest.class);
        assertEquals(expectedTitle, pullRequest.getTitle());
        assertEquals(pullRequest.getCreationDate() + " differs from " +
                expectedDate.getTime(), expectedDate.getTime(), pullRequest.getCreationDate().getTime());
        assertEquals(expectedBody, pullRequest.getBody());
        assertEquals(expectedAuthName, pullRequest.getAuthor().getName());
        assertEquals(expectedAuthAvatarUrl, pullRequest.getAuthor().getAvatarUrl());
        assertEquals(expectedUrl, pullRequest.getUrl());
    }

}
