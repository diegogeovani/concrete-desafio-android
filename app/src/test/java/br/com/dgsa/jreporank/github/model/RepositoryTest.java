package br.com.dgsa.jreporank.github.model;


import android.test.suitebuilder.annotation.SmallTest;

import com.google.gson.Gson;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@SuppressWarnings("unused")
@SmallTest
@RunWith(JUnit4.class)
public class RepositoryTest {

    @SuppressWarnings("TooBroadScope")
    @Test
    public void mustBeDeserializedCorrectly() {
        String json = "{name:\"CS\", description:\"concrete\", " +
                "owner:{avatar_url:\"cs_url.com\", login:\"dgsa\"}, forks: 2, stargazers_count: 3}";
        String expectedName = "CS";
        String expectedDescription = "concrete";
        int expectedForks = 2;
        int expectedStars = 3;
        String expectedOwnerName = "dgsa";
        String expectedAvatarUrl = "cs_url.com";

        Repository repository = new Gson().fromJson(json, Repository.class);
        assertEquals(expectedName, repository.getName());
        assertEquals(expectedDescription, repository.getDescription());
        assertEquals(expectedForks, repository.getNumberOfForks());
        assertEquals(expectedStars, repository.getStars());
        assertEquals(expectedOwnerName, repository.getOwner().getName());
        assertEquals(expectedAvatarUrl, repository.getOwner().getAvatarUrl());
    }

}