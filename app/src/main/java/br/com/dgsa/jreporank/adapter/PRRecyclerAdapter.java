package br.com.dgsa.jreporank.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.PullRequest;
import br.com.dgsa.jreporank.network.HTTPRequestManager;
import br.com.dgsa.jreporank.ui.IntervalOnClickListener;

@SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
public class PRRecyclerAdapter extends RecyclerView.Adapter<PRRecyclerAdapter.ViewHolder> {

    private final List<PullRequest> pullRequests;

    public PRRecyclerAdapter(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View pullRequestItemView = inflater.inflate(R.layout.item_viewgroup_pull_request, parent,
                false);
        return new ViewHolder(pullRequestItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Context context = holder.authorAvatar.getContext().getApplicationContext();
        PullRequest pullRequest = pullRequests.get(position);

        String authorAvatarUrl = pullRequest.getAuthor().getAvatarUrl();
        NetworkImageView authorAvatar = holder.authorAvatar;
        authorAvatar.setImageUrl(authorAvatarUrl, HTTPRequestManager.getInstance(context)
                .getImageLoader());

        PRRecyclerAdapter.displayFormattedCreationDate(holder, pullRequest);
        holder.title.setText(pullRequest.getTitle());
        holder.body.setText(pullRequest.getBody());
        holder.author.setText(context.getString(R.string.by, pullRequest.getAuthor().getName()));

        holder.itemView.setTag(pullRequest);
        holder.itemView.setOnClickListener(new IntervalOnClickListener(new ItemClickEvent()));
    }

    private static void displayFormattedCreationDate(ViewHolder holder, PullRequest pullRequest) {
        String creationDate = pullRequest.getFormattedCreationDate();
        int spaceIndex = creationDate.indexOf(' ');
        if (spaceIndex != -1)
            creationDate = creationDate.substring(0, spaceIndex) + '\n' +
                    creationDate.substring(spaceIndex + 1, creationDate.length());
        holder.date.setText(creationDate);
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    @SuppressWarnings({"unused", "ClassNameSameAsAncestorName"})
    public static final class ViewHolder extends RecyclerView.ViewHolder {

        private final NetworkImageView authorAvatar;
        private final TextView title;
        private final TextView body;
        private final TextView author;
        private final TextView date;

        public ViewHolder(View itemView) {
            super(itemView);
            this.authorAvatar = (NetworkImageView) itemView.findViewById(R.id.image_author_avatar);
            authorAvatar.setDefaultImageResId(R.drawable.ic_person_default);

            this.title = (TextView) itemView.findViewById(R.id.text_pr_title);
            this.body = (TextView) itemView.findViewById(R.id.text_pr_body);
            this.author = (TextView) itemView.findViewById(R.id.text_pr_author);
            this.date = (TextView) itemView.findViewById(R.id.text_pr_date);
        }

    }

    private static final class ItemClickEvent implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            PullRequest pullRequest = (PullRequest) view.getTag();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(pullRequest.getUrl()));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            view.getContext().startActivity(intent);
        }
    }

}
