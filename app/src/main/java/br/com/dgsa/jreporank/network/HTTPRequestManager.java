package br.com.dgsa.jreporank.network;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;

public final class HTTPRequestManager {

    private final RequestQueue requestQueue;

    private final ImageLoader imageLoader;

    private static final int RESPONSES_CACHE_MAX_SIZE = 1024 * 1024 * 2; //2MB

    private static HTTPRequestManager instance;

    public static synchronized HTTPRequestManager getInstance(Context context) {
        if (instance == null)
            instance = new HTTPRequestManager(context);
        return instance;
    }

    private HTTPRequestManager(Context context) {
        Cache responseCache = new DiskBasedCache(context.getCacheDir(), RESPONSES_CACHE_MAX_SIZE);
        Network network = new BasicNetwork(new HurlStack());
        this.requestQueue = new RequestQueue(responseCache, network);
        requestQueue.start();
        Log.i(HTTPRequestManager.class.getSimpleName(), RequestQueue.class.getSimpleName() +
                " initialized");
        this.imageLoader = new ImageLoader(requestQueue, new LruBitmapCache());
    }

    public <T> void execute(Request<T> request) {
        requestQueue.add(request);
    }

    @SuppressWarnings({"SameParameterValue", "unused"})
    public void execute(String imageUrl, ImageLoader.ImageListener listener, int maxWidth,
                        int maxHeight, ImageView.ScaleType scaleType) {
        imageLoader.get(imageUrl, listener, maxWidth, maxHeight, scaleType);
    }

    @NonNull
    public RequestQueue getRequestQueue() {
        return requestQueue;
    }

    @NonNull
    public ImageLoader getImageLoader() {
        return imageLoader;
    }

    private static final class LruBitmapCache extends LruCache<String, Bitmap>
            implements ImageLoader.ImageCache {

        private static final int BITMAP_CACHE_MAX_SIZE = 1024 * 1024 * 6; //6MB

        private LruBitmapCache() {
            super(BITMAP_CACHE_MAX_SIZE);
        }

        @SuppressWarnings("RefusedBequest")
        @Override
        protected int sizeOf(String key, Bitmap value) {
            return value.getRowBytes() * value.getHeight();
        }

        @Override
        public Bitmap getBitmap(String url) {
            return get(url);
        }

        @Override
        public void putBitmap(String url, Bitmap bitmap) {
            put(url, bitmap);
        }
    }

}
