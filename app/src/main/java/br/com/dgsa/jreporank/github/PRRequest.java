package br.com.dgsa.jreporank.github;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import br.com.dgsa.jreporank.github.model.PullRequest;

class PRRequest extends JsonArrayRequest {

    PRRequest(String url, GitHubAPI.PullRequestsCallback callback) {
        this(url, new PRResponse(callback));
    }

    private PRRequest(String url, PRResponse response) {
        super(Method.GET, url, null, response, response);
    }

    private static final class PRResponse implements Response.ErrorListener,
            Response.Listener<JSONArray> {

        private final WeakReference<GitHubAPI.PullRequestsCallback> callbackReference;

        private PRResponse(GitHubAPI.PullRequestsCallback callback) {
            this.callbackReference = new WeakReference<>(callback);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            GitHubAPI.PullRequestsCallback callback = callbackReference.get();
            if (callback != null) {
                Log.e(GitHubAPI.TAG, PRRequest.class.getSimpleName() + " returned an error: " +
                        error.getMessage());
                callback.onGitHubRequestFailed();
                callbackReference.clear();
            }
        }

        @Override
        public void onResponse(JSONArray response) {
            GitHubAPI.PullRequestsCallback callback = callbackReference.get();
            if (callback != null) {
                ArrayList<PullRequest> pullRequests = new ArrayList<>();

                try {
                    for (int i = 0; i < response.length(); i++) {
                        String prJsonString = response.getJSONObject(i).toString();
                        pullRequests.add(new Gson().fromJson(prJsonString, PullRequest.class));
                    }
                    callback.onPullRequestsResponse(pullRequests);

                } catch (JSONException | JsonSyntaxException e) {
                    e.printStackTrace();
                    callback.onGitHubRequestFailed();
                }
                callbackReference.clear();
            }
        }

    }
}
