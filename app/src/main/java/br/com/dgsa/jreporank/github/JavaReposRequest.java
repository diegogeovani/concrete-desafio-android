package br.com.dgsa.jreporank.github;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import br.com.dgsa.jreporank.github.model.Repository;

class JavaReposRequest extends JsonObjectRequest {

    JavaReposRequest(String url, int pageNumber, GitHubAPI.JavaReposCallback callback) {
        this(url + getQueryString(pageNumber), new JavaReposResponse(callback));
    }

    private JavaReposRequest(String url, JavaReposResponse response) {
        super(Method.GET, url, null, response, response);
    }

    @NonNull
    private static String getQueryString(int pageNumber) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("q", "language:Java");
        builder.appendQueryParameter("sort", "starts");
        builder.appendQueryParameter("page", String.valueOf(pageNumber));
        return builder.build().toString();
    }

    private static final class JavaReposResponse implements Response.ErrorListener,
            Response.Listener<JSONObject> {

        private final WeakReference<GitHubAPI.JavaReposCallback> callbackReference;

        private JavaReposResponse(GitHubAPI.JavaReposCallback callback) {
            this.callbackReference = new WeakReference<>(callback);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            GitHubAPI.JavaReposCallback callback = callbackReference.get();
            if (callback != null) {
                Log.e(GitHubAPI.TAG, JavaReposRequest.class.getSimpleName() + " returned an error: " +
                        error.getMessage());
                callback.onGitHubRequestFailed();
                callbackReference.clear();
            }
        }

        @Override
        public void onResponse(JSONObject response) {
            GitHubAPI.JavaReposCallback callback = callbackReference.get();
            if (callback != null) {
                ArrayList<Repository> repositories = new ArrayList<>();

                try {
                    JSONArray reposJsonArray = response.getJSONArray("items");
                    for (int i = 0; i < reposJsonArray.length(); i++) {
                        String repositoryJsonString = reposJsonArray.getJSONObject(i).toString();
                        repositories.add(new Gson().fromJson(repositoryJsonString, Repository.class));
                    }
                    callback.onReposResponse(repositories);

                } catch (JSONException | JsonSyntaxException e) {
                    e.printStackTrace();
                    callback.onGitHubRequestFailed();
                }
                callbackReference.clear();
            }
        }

    }

}
