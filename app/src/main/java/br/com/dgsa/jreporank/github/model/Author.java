package br.com.dgsa.jreporank.github.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Author {

    @SerializedName("login") private String name;
    @SerializedName("avatar_url") private String avatarUrl;

    public String getName() {
        return name;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
