package br.com.dgsa.jreporank.component.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.adapter.DividerItemDecoration;
import br.com.dgsa.jreporank.adapter.PRRecyclerAdapter;
import br.com.dgsa.jreporank.github.GitHubAPI;
import br.com.dgsa.jreporank.github.model.PullRequest;

public class PullRequestActivity extends AppCompatActivity implements GitHubAPI.PullRequestsCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String repositoryName = getIntent().getStringExtra(EXTRAS.REPO_NAME);
        setTitle(repositoryName);
        setContentView(R.layout.activity_pull_request);

        String ownerName = getIntent().getStringExtra(EXTRAS.OWNER_NAME);
        GitHubAPI gitHubAPI = new GitHubAPI();
        gitHubAPI.requestRepoPullRequests(this, repositoryName, ownerName, this);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onGitHubRequestFailed() {
        findViewById(R.id.progress_pull_request).setVisibility(View.GONE);
        TextView message = (TextView) findViewById(R.id.text_message);
        message.setText(R.string.error);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onPullRequestsResponse(@NonNull List<PullRequest> pullRequests) {
        findViewById(R.id.progress_pull_request).setVisibility(View.GONE);

        //noinspection IfStatementWithNegatedCondition
        if (!pullRequests.isEmpty()) {
            PRRecyclerAdapter prRecyclerAdapter = new PRRecyclerAdapter(pullRequests);
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_pull_request);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.inset_divider));
            recyclerView.setAdapter(prRecyclerAdapter);

        } else {
            TextView message = (TextView) findViewById(R.id.text_message);
            message.setText(R.string.no_pull_requests);
        }
    }

    public interface EXTRAS {
        String REPO_NAME = "repository-name";
        String OWNER_NAME = "owner-name";
    }

}
