package br.com.dgsa.jreporank.ui;

import android.os.SystemClock;
import android.view.View;

public final class IntervalOnClickListener implements View.OnClickListener {

    private long lastClickTime;
    private static final int INTERVAL = 1200;

    private final View.OnClickListener onClickListener;

    public IntervalOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onClick(View v) {
        if (!this.isIntervalClickEvent()) {
            this.lastClickTime = SystemClock.elapsedRealtime();
            this.onClickListener.onClick(v);
        }
    }

    private boolean isIntervalClickEvent() {
        return SystemClock.elapsedRealtime() - lastClickTime < INTERVAL;
    }

}
