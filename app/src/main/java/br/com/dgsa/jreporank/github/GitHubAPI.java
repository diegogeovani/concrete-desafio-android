package br.com.dgsa.jreporank.github;

import android.content.Context;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.PullRequest;
import br.com.dgsa.jreporank.github.model.Repository;
import br.com.dgsa.jreporank.network.HTTPRequestManager;

@SuppressWarnings("ClassNamePrefixedWithPackageName")
public class GitHubAPI {

    static final String TAG = GitHubAPI.class.getSimpleName();

    @SuppressWarnings("MethodMayBeStatic")
    public void requestJavaRepos(Context context, @IntRange(from = 1) int pageNumber,
                                 JavaReposCallback callback) {
        JavaReposRequest javaReposRequest = new JavaReposRequest(
                context.getString(R.string.url_github_repositories), pageNumber, callback);
        HTTPRequestManager httpRequestManager = HTTPRequestManager.getInstance(context);
        httpRequestManager.execute(javaReposRequest);
    }

    @SuppressWarnings("MethodMayBeStatic")
    public void requestRepoPullRequests(Context context, String repository, String owner,
                                        PullRequestsCallback callback) {
        String url = context.getString(R.string.url_github_pull_requests, owner, repository);
        Log.i(TAG, "PR request url: " + url);
        PRRequest prRequest = new PRRequest(url, callback);
        HTTPRequestManager httpRequestManager = HTTPRequestManager.getInstance(context);
        httpRequestManager.execute(prRequest);
    }

    interface GitHubCallback {
        void onGitHubRequestFailed();
    }

    public interface JavaReposCallback extends GitHubCallback {
        void onReposResponse(@NonNull List<Repository> repositories);
    }

    public interface PullRequestsCallback extends GitHubCallback {
        void onPullRequestsResponse(@NonNull List<PullRequest> pullRequests);
    }

}
