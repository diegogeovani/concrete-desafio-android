package br.com.dgsa.jreporank.component.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.adapter.DividerItemDecoration;
import br.com.dgsa.jreporank.adapter.RepositoryRecyclerAdapter;
import br.com.dgsa.jreporank.github.GitHubAPI;
import br.com.dgsa.jreporank.github.model.Repository;

public class MainActivity extends AppCompatActivity implements GitHubAPI.JavaReposCallback {

    private RecyclerView recyclerView;
    private int pageNumber;
    private boolean scrollLoad = true;

    private GitHubAPI gitHubAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_main);
        this.gitHubAPI = new GitHubAPI();
        gitHubAPI.requestJavaRepos(getApplicationContext(), pageNumber + 1, this);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onGitHubRequestFailed() {
        findViewById(R.id.progress_main).setVisibility(View.GONE);
        //noinspection rawtypes
        RecyclerView.Adapter adapter = recyclerView.getAdapter();
        if (adapter == null || adapter.getItemCount() == 0) {
            TextView message = (TextView) findViewById(R.id.text_message);
            message.setText(R.string.error);

        } else if (adapter != null && adapter.getItemCount() > 0)
            findViewById(R.id.progress_scroll).setVisibility(View.GONE);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onReposResponse(@NonNull List<Repository> repositories) {
        findViewById(R.id.progress_main).setVisibility(View.GONE);
        findViewById(R.id.progress_scroll).setVisibility(View.GONE);

        //noinspection IfStatementWithNegatedCondition
        if (!repositories.isEmpty()) {
            if (pageNumber == 0) {
                RepositoryRecyclerAdapter recyclerAdapter = new RepositoryRecyclerAdapter(repositories);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
                recyclerView.addItemDecoration(new DividerItemDecoration(this,
                        R.drawable.inset_divider));
                recyclerView.setAdapter(recyclerAdapter);
                recyclerView.addOnScrollListener(new EndlessScrollListener());

            } else {
                RepositoryRecyclerAdapter recyclerAdapter = (RepositoryRecyclerAdapter)
                        recyclerView.getAdapter();
                recyclerAdapter.addAll(repositories);
                this.scrollLoad = true;
            }
            pageNumber++;
            if (pageNumber > 1)
                Log.d(MainActivity.class.getSimpleName(), "Page " + pageNumber + " added to the list");

        } else {
            TextView message = (TextView) findViewById(R.id.text_message);
            message.setText(R.string.no_repos);
        }

    }

    private final class EndlessScrollListener extends RecyclerView.OnScrollListener {

        private int visibleItemsCount;
        private int totalItemsCount;
        private int pastVisibleItems;

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            if (dy > 0) {
                this.visibleItemsCount = layoutManager.getChildCount();
                this.totalItemsCount = layoutManager.getItemCount();
                this.pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                if (scrollLoad && isLastItemVisible()) {
                    scrollLoad = false;
                    //noinspection ConstantConditions
                    findViewById(R.id.progress_scroll).setVisibility(View.VISIBLE);
                    gitHubAPI.requestJavaRepos(getApplicationContext(), pageNumber + 1,
                            MainActivity.this);
                }
            }
        }

        private boolean isLastItemVisible() {
            return visibleItemsCount + pastVisibleItems >= totalItemsCount;
        }

    }

}
