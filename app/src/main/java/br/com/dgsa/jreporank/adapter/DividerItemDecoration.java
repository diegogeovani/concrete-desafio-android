package br.com.dgsa.jreporank.adapter;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private final Drawable divider;
    @SuppressWarnings("UnnecessaryFullyQualifiedName")
    private static final int[] DIVIDER_ATTRS = {android.R.attr.listDivider};

    @SuppressWarnings("unused")
    public DividerItemDecoration(Context context) {
        //noinspection UnnecessaryFinalOnLocalVariableOrParameter
        final TypedArray styledAttributes = context.obtainStyledAttributes(DIVIDER_ATTRS);
        divider = styledAttributes.getDrawable(0);
        styledAttributes.recycle();
    }

    @SuppressWarnings("SameParameterValue")
    public DividerItemDecoration(Context context, @DrawableRes int resId) {
        divider = ContextCompat.getDrawable(context, resId);
    }

    @SuppressWarnings("RefusedBequest")
    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + divider.getIntrinsicHeight();

            divider.setBounds(left, top, right, bottom);
            divider.draw(c);
        }
    }

}
