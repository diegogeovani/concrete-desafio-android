package br.com.dgsa.jreporank.github.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@SuppressWarnings("unused")
public class PullRequest {

    private String title;
    @SerializedName("html_url") private String url;
    @SerializedName("created_at") private Date creationDate;
    private String body;
    @SerializedName("user") private Author author;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm",
            new Locale("pt", "BR"));

    public String getTitle() {
        return title;
    }

    @SuppressWarnings("AccessToNonThreadSafeStaticField")
    public String getFormattedCreationDate() {
        return dateFormat.format(creationDate);
    }

    public Date getCreationDate() {
        return (Date) creationDate.clone();
    }

    public String getBody() {
        return body;
    }

    public Author getAuthor() {
        return author;
    }

    public String getUrl() {
        return url;
    }

}
