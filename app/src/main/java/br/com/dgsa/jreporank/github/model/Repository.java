package br.com.dgsa.jreporank.github.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Repository {

    private String name;
    private String description;
    private Owner owner;
    @SerializedName("forks") private int numberOfForks;
    @SerializedName("stargazers_count") private int stars;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Owner getOwner() {
        return owner;
    }

    public int getNumberOfForks() {
        return numberOfForks;
    }

    public int getStars() {
        return stars;
    }

}
