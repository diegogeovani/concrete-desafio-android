package br.com.dgsa.jreporank.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;

import java.util.List;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.component.activity.PullRequestActivity;
import br.com.dgsa.jreporank.github.model.Repository;
import br.com.dgsa.jreporank.network.HTTPRequestManager;
import br.com.dgsa.jreporank.ui.IntervalOnClickListener;

@SuppressWarnings("AccessingNonPublicFieldOfAnotherObject")
public class RepositoryRecyclerAdapter
        extends RecyclerView.Adapter<RepositoryRecyclerAdapter.ViewHolder> {

    private final List<Repository> repositories;

    public RepositoryRecyclerAdapter(List<Repository> repositories) {
        this.repositories = repositories;
    }

    public void addAll(List<Repository> repositories) {
        this.repositories.addAll(repositories);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View repositoryItemView = inflater.inflate(R.layout.item_viewgroup_repository, parent, false);
        return new ViewHolder(repositoryItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Context context = holder.ownerAvatar.getContext().getApplicationContext();
        Repository repository = repositories.get(position);

        String ownerAvatarUrl = repository.getOwner().getAvatarUrl();
        NetworkImageView ownerAvatar = holder.ownerAvatar;
        ownerAvatar.setImageUrl(ownerAvatarUrl, HTTPRequestManager.getInstance(context)
                .getImageLoader());

        holder.repoName.setText(repository.getName());
        holder.repoDescription.setText(repository.getDescription());
        holder.owner.setText(repository.getOwner().getName());
        holder.numberOfForks.setText(String.valueOf(repository.getNumberOfForks()));
        holder.stars.setText(String.valueOf(repository.getStars()));

        holder.itemView.setTag(repository);
        holder.itemView.setOnClickListener(new IntervalOnClickListener(new ItemClickEvent()));
    }

    @Override
    public int getItemCount() {
        return repositories.size();
    }

    @SuppressWarnings("ClassNameSameAsAncestorName")
    public static final class ViewHolder extends RecyclerView.ViewHolder {

        private final NetworkImageView ownerAvatar;
        private final TextView repoName;
        private final TextView repoDescription;
        private final TextView owner;
        private final TextView numberOfForks;
        private final TextView stars;

        public ViewHolder(View itemView) {
            super(itemView);
            this.ownerAvatar = (NetworkImageView) itemView.findViewById(R.id.image_owner_avatar);
            ownerAvatar.setDefaultImageResId(R.drawable.ic_person_default);

            this.repoName = (TextView) itemView.findViewById(R.id.text_repo_name);
            this.repoDescription = (TextView) itemView.findViewById(R.id.text_repo_description);
            this.owner = (TextView) itemView.findViewById(R.id.text_owner);
            this.numberOfForks = (TextView) itemView.findViewById(R.id.text_number_of_forks);
            this.stars = (TextView) itemView.findViewById(R.id.text_repo_stars);
        }
    }

    private static final class ItemClickEvent implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Repository repository = (Repository) v.getTag();
            Intent pullRequestIntent = new Intent(v.getContext(), PullRequestActivity.class);
            pullRequestIntent.putExtra(PullRequestActivity.EXTRAS.REPO_NAME, repository.getName());
            pullRequestIntent.putExtra(PullRequestActivity.EXTRAS.OWNER_NAME, repository.getOwner().getName());
            pullRequestIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            v.getContext().startActivity(pullRequestIntent);
        }
    }

}
