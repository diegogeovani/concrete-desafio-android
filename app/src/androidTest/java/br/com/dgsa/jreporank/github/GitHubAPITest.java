package br.com.dgsa.jreporank.github;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import br.com.dgsa.jreporank.github.model.PullRequest;
import br.com.dgsa.jreporank.github.model.Repository;

@SuppressWarnings({"unused", "ClassNamePrefixedWithPackageName"})
@RunWith(AndroidJUnit4.class)
public class GitHubAPITest {

    @SuppressWarnings({"AnonymousInnerClassWithTooManyMethods", "AnonymousInnerClassMayBeStatic"})
    @Test
    public void requestJavaRepos() {
        Context context = InstrumentationRegistry.getTargetContext();
        GitHubAPI gitHubApi = new GitHubAPI();
        gitHubApi.requestJavaRepos(context, 1, new GitHubAPI.JavaReposCallback() {
            @Override
            public void onGitHubRequestFailed() {

            }

            @Override
            public void onReposResponse(@NonNull List<Repository> repositories) {

            }
        });
    }

    @Test
    public void requestRepoPullRequests() {
        Context context = InstrumentationRegistry.getTargetContext();
        String repoName = "some repo";
        String owner = "owner name";
        GitHubAPI gitHubApi = new GitHubAPI();
        //noinspection AnonymousInnerClassWithTooManyMethods,AnonymousInnerClassMayBeStatic
        gitHubApi.requestRepoPullRequests(context, repoName, owner,
                new GitHubAPI.PullRequestsCallback() {
            @Override
            public void onPullRequestsResponse(@NonNull List<PullRequest> pullRequests) {

            }

            @Override
            public void onGitHubRequestFailed() {

            }
        });
    }

}
