package br.com.dgsa.jreporank.network;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.StringRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNotNull;

@SuppressWarnings("unused")
@SmallTest
@RunWith(AndroidJUnit4.class)
public class HTTPRequestManagerTest {

    private Context context;

    @Before
    public void setUp() {
        this.context = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void getRequestQueueMustMustNotReturnNull() {
        HTTPRequestManager httpRequestManager = HTTPRequestManager.getInstance(context);
        RequestQueue requestQueue = httpRequestManager.getRequestQueue();
        assertNotNull("Null request queue", requestQueue);
    }

    @Test
    public void getImageLoaderMustNotReturnNull() {
        HTTPRequestManager httpRequestManager = HTTPRequestManager.getInstance(context);
        ImageLoader imageLoader = httpRequestManager.getImageLoader();
        assertNotNull("Null image loader", imageLoader);
    }

    @Test
    public void execute() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                "http://google.com.br", null, null);
        HTTPRequestManager httpRequestManager = HTTPRequestManager.getInstance(context);
        httpRequestManager.execute(stringRequest);
    }

}
