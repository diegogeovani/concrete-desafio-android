package br.com.dgsa.jreporank.component.activity;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.Repository;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@SuppressWarnings("unused")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityIntegrationTest {

    @Rule
    public final ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class, false, false);
    private MainActivity activity;

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), MainActivity.class);
        this.activity = activityTestRule.launchActivity(intent);
    }

    @Test
    public void mustHaveContainerId() {
        onView(ViewMatchers.withId(R.id.container)).check(matches(isDescendantOfA(isRoot())));
    }

    @Test
    public void mustHaveProgressBar() {
        onView(withId(R.id.progress_main)).check(matches(is(instanceOf(ProgressBar.class))));
    }

    @Test
    public void progressBarMustBeGoneOnGitHubCallback() throws Throwable {
        activityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.onReposResponse(new ArrayList<Repository>());
            }
        });
        onView(withId(R.id.progress_main)).check(matches(not(isDisplayed())));
    }

    @Test
    public void mustHaveARecyclerView() {
        onView(withId(R.id.recycler_main)).check(matches(is(instanceOf(RecyclerView.class))));
    }

    @After
    public void tearDown() {
        activity.finish();
    }

}
