package br.com.dgsa.jreporank.component.activity;

import android.app.Instrumentation;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.PullRequest;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertEquals;

@SuppressWarnings("unused")
@LargeTest
@RunWith(AndroidJUnit4.class)
public class PullRequestActivityIntegrationTest {

    @Rule
    public final ActivityTestRule<PullRequestActivity> activityTestRule =
            new ActivityTestRule<>(PullRequestActivity.class, false, false);
    private PullRequestActivity activity;

    @Test
    public void mustHaveContainerId() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), PullRequestActivity.class);
        this.activity = activityTestRule.launchActivity(intent);
        ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progress_pull_request);
        //noinspection ConstantConditions,deprecation
        progressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.mipmap.ic_launcher));

        onView(withId(R.id.container)).check(matches(isDescendantOfA(isRoot())));
    }

    @Test
    public void mustSetRepoNameAsTitle() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), PullRequestActivity.class);
        String expected = "some repo";
        intent.putExtra(PullRequestActivity.EXTRAS.REPO_NAME, expected);
        this.activity = activityTestRule.launchActivity(intent);

        assertEquals("activity must set repo name as title", expected, activity.getTitle());
    }

    @Test
    public void mustHaveProgressBar() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), PullRequestActivity.class);
        this.activity = activityTestRule.launchActivity(intent);
        ProgressBar progressBar = (ProgressBar) activity.findViewById(R.id.progress_pull_request);
        //noinspection ConstantConditions,deprecation
        progressBar.setIndeterminateDrawable(activity.getResources().getDrawable(R.mipmap.ic_launcher));

        onView(withId(R.id.progress_pull_request)).check(matches(is(instanceOf(ProgressBar.class))));
    }

    @Test
    public void progressBarMustBeGoneOnGitHubCallback() throws Throwable {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), PullRequestActivity.class);
        this.activity = activityTestRule.launchActivity(intent);
        activityTestRule.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.onPullRequestsResponse(new ArrayList<PullRequest>());
            }
        });
        onView(withId(R.id.progress_pull_request)).check(matches(not(isDisplayed())));
    }

    @Test
    public void mustHaveARecyclerView() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        Intent intent = new Intent(instrumentation.getContext(), PullRequestActivity.class);
        this.activity = activityTestRule.launchActivity(intent);
        onView(withId(R.id.recycler_pull_request)).check(matches(is(instanceOf(RecyclerView.class))));
    }

    @After
    public void tearDown() {
        if (activity != null)
            activity.finish();
    }

}
