package br.com.dgsa.jreporank.adapter;

import android.annotation.SuppressLint;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.PullRequest;
import br.com.dgsa.jreporank.widget.RoundedNetworkImageView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(AndroidJUnit4.class)
public class PRRecyclerAdapterTest {

    @Test
    public void getItemCountMustReturnArraySize() {
        ArrayList<PullRequest> pullRequests = new ArrayList<>();
        pullRequests.add(new PullRequest());
        pullRequests.add(new PullRequest());
        PRRecyclerAdapter prRecyclerAdapter = new PRRecyclerAdapter(pullRequests);
        int expected = pullRequests.size();
        int actual = prRecyclerAdapter.getItemCount();
        assertEquals("Adapter item count must be the same as the array size", expected, actual);
    }

    @Test
    public void itemViewMustHaveSpecificViews() {
        LayoutInflater inflater = LayoutInflater.from(InstrumentationRegistry.getTargetContext());
        @SuppressLint("InflateParams")
        View pullRequestItemView = inflater.inflate(R.layout.item_viewgroup_pull_request, null, false);
        RoundedNetworkImageView authorAvatar = (RoundedNetworkImageView) pullRequestItemView
                .findViewById(R.id.image_author_avatar);
        TextView prTitle = (TextView) pullRequestItemView.findViewById(R.id.text_pr_title);
        TextView prBody = (TextView) pullRequestItemView.findViewById(R.id.text_pr_body);
        TextView author = (TextView) pullRequestItemView.findViewById(R.id.text_pr_author);
        TextView prDate = (TextView) pullRequestItemView.findViewById(R.id.text_pr_date);

        assertNotNull(pullRequestItemView);
        assertNotNull(authorAvatar);
        assertNotNull(prTitle);
        assertNotNull(prBody);
        assertNotNull(author);
        assertNotNull(prDate);
    }

    @Test
    public void itemViewAuthorViewMustAllowOnlyOneTextLine() {
        LayoutInflater inflater = LayoutInflater.from(InstrumentationRegistry.getTargetContext());
        @SuppressLint("InflateParams")
        View pullRequestItemView = inflater.inflate(R.layout.item_viewgroup_pull_request, null, false);
        TextView author = (TextView) pullRequestItemView.findViewById(R.id.text_pr_author);

        String message = "view must allow only 1 line and ellipsize its text";
        int expectedMaxLines = 1;
        TextUtils.TruncateAt expectedTruncation = TextUtils.TruncateAt.END;
        assertTrue(message, author.getMaxLines() == expectedMaxLines &&
                author.getEllipsize() == expectedTruncation);
    }

}
