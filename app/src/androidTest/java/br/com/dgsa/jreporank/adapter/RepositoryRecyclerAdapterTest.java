package br.com.dgsa.jreporank.adapter;

import android.annotation.SuppressLint;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import br.com.dgsa.jreporank.R;
import br.com.dgsa.jreporank.github.model.Repository;
import br.com.dgsa.jreporank.widget.RoundedNetworkImageView;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@SuppressWarnings("unused")
@RunWith(AndroidJUnit4.class)
public class RepositoryRecyclerAdapterTest {

    @Test
    public void getItemCountMustReturnArraySize() {
        ArrayList<Repository> repositories = new ArrayList<>();
        repositories.add(new Repository());
        repositories.add(new Repository());
        RepositoryRecyclerAdapter repositoryRecyclerAdapter =
                new RepositoryRecyclerAdapter(repositories);
        int expected = repositories.size();
        assertEquals("Adapter item count must be the same as the array size", expected,
                repositoryRecyclerAdapter.getItemCount());
    }

    @Test
    public void itemViewMustHaveSpecificViews() {
        LayoutInflater inflater = LayoutInflater.from(InstrumentationRegistry.getTargetContext());
        @SuppressLint("InflateParams")
        View repositoryItemView = inflater.inflate(R.layout.item_viewgroup_repository, null, false);
        RoundedNetworkImageView ownerAvatar = (RoundedNetworkImageView) repositoryItemView
                .findViewById(R.id.image_owner_avatar);
        TextView repoName = (TextView) repositoryItemView.findViewById(R.id.text_repo_name);
        TextView repoDescription = (TextView) repositoryItemView
                .findViewById(R.id.text_repo_description);
        TextView owner = (TextView) repositoryItemView.findViewById(R.id.text_owner);
        ImageView forkIcon = (ImageView) repositoryItemView.findViewById(R.id.image_fork_icon);
        TextView numberOfForks = (TextView) repositoryItemView.findViewById(R.id.text_number_of_forks);
        ImageView starIcon = (ImageView) repositoryItemView.findViewById(R.id.image_star_icon);
        TextView stars = (TextView) repositoryItemView.findViewById(R.id.text_repo_stars);

        assertNotNull(ownerAvatar);
        assertNotNull(repoName);
        assertNotNull(repoDescription);
        assertNotNull(owner);
        assertNotNull(forkIcon);
        assertNotNull(numberOfForks);
        assertNotNull(starIcon);
        assertNotNull(stars);
    }

    @Test
    public void itemViewTextViewsMustAllowOnlyOneTextLine() {
        LayoutInflater inflater = LayoutInflater.from(InstrumentationRegistry.getTargetContext());
        @SuppressLint("InflateParams")
        View repositoryItemView = inflater.inflate(R.layout.item_viewgroup_repository, null, false);
        TextView repoName = (TextView) repositoryItemView.findViewById(R.id.text_repo_name);
        TextView repoDescription = (TextView) repositoryItemView
                .findViewById(R.id.text_repo_description);
        TextView owner = (TextView) repositoryItemView.findViewById(R.id.text_owner);
        TextView numberOfForks = (TextView) repositoryItemView.findViewById(R.id.text_number_of_forks);
        TextView stars = (TextView) repositoryItemView.findViewById(R.id.text_repo_stars);

        String message = "view must allow only 1 line and ellipsize its text";
        int expectedMaxLines = 1;
        TextUtils.TruncateAt expectedTruncation = TextUtils.TruncateAt.END;
        assertTrue(message, repoName.getMaxLines() == expectedMaxLines &&
                repoName.getEllipsize() == expectedTruncation);
        assertTrue(message, repoDescription.getMaxLines() == expectedMaxLines &&
                repoDescription.getEllipsize() == expectedTruncation);
        assertTrue(message, owner.getMaxLines() == expectedMaxLines &&
                owner.getEllipsize() == expectedTruncation);
        assertTrue(message, numberOfForks.getMaxLines() == expectedMaxLines &&
                numberOfForks.getEllipsize() == expectedTruncation);
        assertTrue(message, stars.getMaxLines() == expectedMaxLines &&
                stars.getEllipsize() == expectedTruncation);
    }

}
